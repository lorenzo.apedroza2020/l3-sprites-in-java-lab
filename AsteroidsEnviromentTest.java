/*****************************************************
* 022019@LorenzoPedroza
* L3: Sprites in Java Lab Program
* Built off of SpriteTest program by Jonathan S. Harbour
*****************************************************/

import java.awt.*;
import java.awt.image.*;
import java.applet.*;
import java.util.*;
import java.net.*;

public class AsteroidsEnviromentTest extends Applet implements Runnable {
    private static final int SCREENWIDTH = 640;
    private static final int SCREENHEIGHT = 480;

    private static final int NUMBEROFASTEROIDS = 5;
    private static final int NUMBEROFEXPLOSIONS = 20;
    private static int numberOfAvailibleExplosions = NUMBEROFASTEROIDS;

	//double buffer objects
    BufferedImage backbuffer;
    Graphics2D g2d;

    Sprite[] asteroids = new Sprite[NUMBEROFASTEROIDS];
    Sprite spaceship;
    
    ImageEntity background;
    Thread gameloop;
    Random rand = new Random();

    AnimatedSprite[] explosions = new AnimatedSprite[20]; //going to need to implement more dyniamic array style in future
    AnimatedSprite blackHole;


    public void init() {
 		//create the back buffer for smooth graphics
        backbuffer = new BufferedImage(SCREENWIDTH, SCREENHEIGHT,
            BufferedImage.TYPE_INT_RGB);
        g2d = backbuffer.createGraphics();

        for(int i = 0; i < explosions.length; i++) //Lorrnzo Pedroza
        {
            explosions[i] = new AnimatedSprite(this, g2d);
            explosions[i].setAlive(false);
            explosions[i].load("explosion.png", 4, 4, 96, 96);
            explosions[i].setFrameDelay(2);
            explosions[i].setAnimationLooping(false);
        }

        blackHole = new AnimatedSprite(this, g2d);
        blackHole.setAlive(true);
        blackHole.load("blackhole64x64x16.png", 4, 4, 64, 64);
        blackHole.setAnimationLooping(true);
        blackHole.setPosition(new Point2D(SCREENWIDTH/2 - blackHole.imageWidth()+20, SCREENHEIGHT/2 - blackHole.imageHeight()+20));

        spaceship = new Sprite(this, g2d);
        spaceship.load("PedrozaL_Spaceship.png");
        spaceship.setAlive(true);
        spaceship.setPosition(new Point2D(rand.nextInt(SCREENWIDTH), rand.nextInt(SCREENHEIGHT)));
        spaceship.setFaceAngle(rand.nextInt(360)); //360 degrees in circle
        spaceship.setMoveAngle(rand.nextInt(360));
        spaceship.setRotationRate(rand.nextDouble() + 0.1);
        spaceship.setScale(0.1, 0.1);
        
        double angspaceship = spaceship.moveAngle() - 90; //confunctions 90 degrees offset
        double spacevelx = calcAngleMoveX(angspaceship);
        double spacevely = calcAngleMoveY(angspaceship);
        spaceship.setVelocity(new Point2D(spacevelx, spacevely));

           

		//load the background
		background = new ImageEntity(this);
        background.load("bluespace.png");

        //fill the array with asteroid sprites so we can use for each loops
        for (int i = 0; i < NUMBEROFASTEROIDS; i++)
            asteroids[i] = new Sprite(this, g2d);

        //load the asteroid sprites
        for (Sprite asteroid : asteroids){ //Lorenzo Pedroza
            asteroid.load("asteroid2.png");
            asteroid.setAlive(true);
            int x = rand.nextInt(SCREENWIDTH);
            int y = rand.nextInt(SCREENHEIGHT);
            asteroid.setPosition(new Point2D(x, y)); //TODO see if checking if veryfying unique starting location is necesarry.
            
            asteroid.setFaceAngle(rand.nextInt(360)); //360 degrees in circle
            asteroid.setMoveAngle(rand.nextInt(360));
            asteroid.setRotationRate(rand.nextDouble() + 0.1);
            
            double ang = asteroid.moveAngle() - 90; //confunctions 90 degrees offset
            double velx = calcAngleMoveX(ang);
            double vely = calcAngleMoveY(ang);
            asteroid.setVelocity(new Point2D(velx, vely));

            double scale = (rand.nextDouble()+0.1);
            
            asteroid.setScale(scale, scale);
        }
        
    }

    public void start() {
        gameloop = new Thread(this);
        gameloop.start();
    }

    public void stop() {
        gameloop = null;
    }

    public void run() {
        Thread t = Thread.currentThread();
        while (t == gameloop) {
            try {
                Thread.sleep(30);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            
            repaint();
            checkCollisions();
            for(AnimatedSprite explosion: explosions)
            if(explosion.alive())
                explosion.updateAnimation();
            blackHole.updateAnimation();
        }
    }

    public void update(Graphics g) {
		//draw the background
        g2d.drawImage(background.getImage(), 0, 0, SCREENWIDTH-1, SCREENHEIGHT-1, this);

         /*
        int width = screenWidth - asteroid.imageWidth() - 1;
        int height = screenHeight - asteroid.imageHeight() - 1;
		Point2D point = new Point2D(rand.nextInt(width), rand.nextInt(height));
        asteroid.setPosition(point);
        */
        updateAsteroids();
        updateSpaceShip();
        drawAsteroids();
        drawExplosions();
        drawBlackHole();
        drawSpaceship();

        paint(g);
    }

    public void paint(Graphics g) {
		//draw the back buffer to the screen
        g.drawImage(backbuffer, 0, 0, this);
    }

    private double calcAngleMoveX(double angle){
        return Math.cos(angle*Math.PI/180); //physics: For finding x use cosine
    }

    private double calcAngleMoveY(double angle){
        return Math.sin(angle*Math.PI/180); //physics: For finding y use sine.
    }

    private void drawAsteroids(){ //move asteroid to new positions (calculuted from their assigned velcoity) and draw them.
        for(Sprite asteroid : asteroids){
            if(asteroid.alive()){
                asteroid.transform();
                asteroid.draw();
                asteroid.drawBounds(Color.GREEN); //debugging
            }
            
        }
    }

    private void updateAsteroids(){ //update the Sprite fields of each asteroid.
        for(Sprite asteroid : asteroids){
            if(asteroid.alive()) //do not bother with dead asteroids
            {
                asteroid.updatePosition();
                asteroid.updateRotation();
    
                double w = asteroid.imageWidth() - 1; //get the asteroid imagewidth
                double h = asteroid.imageHeight() - 1; //get the asteroid imageheight

                double newx = asteroid.position().X();
                double newy = asteroid.position().Y();


                //handle screen edge wrapping
                if(asteroid.position().X() < -w) //check the left side. make sure asteroid is completel out of frame
                    newx = SCREENWIDTH + w;
                
                else if (asteroid.position().X() > SCREENWIDTH + w) //check the right side; make sure asteroid is completely out of frame
                    newx = -w;
                
                if(asteroid.position().Y() < -h) //check top; same principle
                    newy = SCREENHEIGHT + h;

                else if(asteroid.position().Y() > SCREENHEIGHT + h) //check bottom; same principle
                    newy = -h;
                
                asteroid.setPosition(new Point2D(newx , newy));

            }
            
        }
    }

    private void updateSpaceShip(){ //update the Sprite fields of each asteroid.
        
        if(spaceship.alive()) //do not bother with dead asteroids
        {
            spaceship.updatePosition();
            spaceship.updateRotation();

            double w = spaceship.imageWidth() - 1; //get the asteroid imagewidth
            double h = spaceship.imageHeight() - 1; //get the asteroid imageheight

            double newx = spaceship.position().X();
            double newy = spaceship.position().Y();


            //handle screen edge wrapping
            if(spaceship.position().X() < -w) //check the left side. make sure asteroid is completel out of frame
                newx = SCREENWIDTH + w;
            
            else if (spaceship.position().X() > SCREENWIDTH + w) //check the right side; make sure asteroid is completely out of frame
                newx = -w;
            
            if(spaceship.position().Y() < -h) //check top; same principle
                newy = SCREENHEIGHT + h;

            else if(spaceship.position().Y() > SCREENHEIGHT + h) //check bottom; same principle
                newy = -h;
            
            spaceship.setPosition(new Point2D(newx , newy));

        }
            
    }

    private void drawExplosions(){
        for(AnimatedSprite explosion : explosions){
            if(explosion.alive())
            {
                explosion.updateFrame();
                explosion.transform();
                explosion.draw();
                explosion.drawBounds(Color.RED); //debugging
            }
        }
    }

    private void drawBlackHole(){
        blackHole.updateFrame();
        blackHole.transform();
        blackHole.draw();
        blackHole.drawBounds(Color.WHITE);
    }

    private void drawSpaceship(){
        if(spaceship.alive())
        {
            spaceship.transform();
            spaceship.draw();
            spaceship.drawBounds(Color.YELLOW);
        }  
    }

    public void generateExplosion(Point2D a){
        AnimatedSprite explosion = new AnimatedSprite(this, g2d);
        numberOfAvailibleExplosions--;
        if(numberOfAvailibleExplosions >= 0)
        {
            explosions[numberOfAvailibleExplosions].setAlive(true);
            explosions[numberOfAvailibleExplosions].setPosition(a);
        }
    }


    private void checkCollisions()
    {
        for(int firstSpriteIndex = 0; firstSpriteIndex < asteroids.length; firstSpriteIndex++)  //need to use array indexes. So not using a for each loop here.
        {
            Sprite currentAsterioid = asteroids[firstSpriteIndex];
            if(currentAsterioid.alive())
            {
                for(int secondSpriteIndex = 0; secondSpriteIndex < asteroids.length; secondSpriteIndex++)//This block checks and handles asteroid collisions with themselves
                {
                    if(firstSpriteIndex != secondSpriteIndex) //ignore itself
                    {
                        Sprite asteroidToCompareTo = asteroids[secondSpriteIndex];
                        if(asteroidToCompareTo.alive()) //make sure it is alive
                            if(asteroidToCompareTo.collidesWith(currentAsterioid))
                            {
                               //get midpoint between two asteroids and generate explosion from that point. Try to make bigger for bigger asteroid collisons
                               Point2D midPointOfAsteroids = new Point2D(((currentAsterioid.position().X() + asteroidToCompareTo.position().X())/2),
                                ((currentAsterioid.position().Y() + asteroidToCompareTo.position().Y())/2));

                               generateExplosion(midPointOfAsteroids);
                               currentAsterioid.setAlive(false);
                               asteroidToCompareTo.setAlive(false);
                            }
                    }
                }

                if(blackHole.collidesWith(currentAsterioid)){
                    currentAsterioid.setAlive(false);
                    blackHole.setScale(blackHole.getScaleX() + 0.7 , blackHole.getScaleY()+ 0.7);

                }
            

                if(spaceship.collidesWith(currentAsterioid)){

                    if(spaceship.alive())
                    {
                        //get midpoint between two asteroids and generate explosion from that point. Try to make bigger for bigger asteroid collisons
                        Point2D midPointOfSprites = new Point2D(((currentAsterioid.position().X() + spaceship.position().X())/2),
                        ((currentAsterioid.position().Y() + spaceship.position().Y())/2));
                        generateExplosion(midPointOfSprites);
                        currentAsterioid.setAlive(false);
                        spaceship.setAlive(false);

                    }
                   
                }


            }

        }

        if(blackHole.collidesWith(spaceship)){

            if(spaceship.alive())
            {
                blackHole.setScale(blackHole.getScaleX() + 0.7 , blackHole.getScaleY()+ 0.7);
                spaceship.setAlive(false);
            }
               

        }
    }
}
