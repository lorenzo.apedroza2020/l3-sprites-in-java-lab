/*********************************************************
 * Base game image class for bitmapped game entities
 * Modified 030719@LorenzoPedroza to enable scaling functionality of bitmapped game entitities
 **********************************************************/
import java.awt.*;
import java.awt.geom.*;
import java.net.*;
import java.applet.*;

public class ImageEntity extends BaseGameEntity {
    //variables
    protected Image image;
    protected Applet applet;
    protected AffineTransform at;
    protected Graphics2D g2d;

    //default constructor
    ImageEntity(Applet a) {
        applet = a;
        setImage(null);
        setAlive(true);
    }

    public Image getImage() { return image; }

    public void setImage(Image image) {
        this.image = image;
        double x = applet.getSize().width/2  - width()/2;
        double y = applet.getSize().height/2 - height()/2;
        at = AffineTransform.getTranslateInstance(x, y);
    }

    //Code to correct translation movement. I ran out of time to correct this with black hole and all sprites. Nonthelss, code works

    /* not fully funcitonal 
    @Override //need to account for scaling an image. This keeps the x centered as if it were the orignal and corrects artifacts of scaling using AffineTransform Scaling
    public double getX() {
        return super.getX()  - 0.5*(width() - width()/getScaleX()); //scaled width minus the orignal width 
    }

    @Override //need to account for scaling an image
    public double getY() {
        return super.getY() - 0.5*(height() - height()/getScaleX());
    }
    */

    public int width() {
        if (image != null)
            return (int)(image.getWidth(applet)*scaleX);
        else
            return 0;
    }
    public int height() {
        if (image != null)
            return (int)(image.getHeight(applet)*scaleY);
        else
            return 0;
    }

    public double getCenterX() { //modificed to account for graphics scaling
        return getX() + width() / 2;
    }
    public double getCenterY() {
        return getY() + height() / 2;
    }

    public double getScaleX(){return at.getScaleX();}
    public double getScaleY(){return at.getScaleY();}

    public void setGraphics(Graphics2D g) {
        g2d = g;
    }

//*************
    private URL getURL(String filename) {
        URL url = null;
        try {
            url = this.getClass().getResource(filename);
            //url = new java.net.URL(applet.getCodeBase() + filename);
        }
        //catch (MalformedURLException e) { e.printStackTrace(); }
        catch (Exception e) { }

        return url;
    }

    public void load(String filename) {
        image = applet.getImage(getURL(filename));
        while(getImage().getWidth(applet) <= 0);
        double x = applet.getSize().width/2  - width()/2;
        double y = applet.getSize().height/2 - height()/2;
        at = AffineTransform.getTranslateInstance(x, y);
    }

    public void transform() {
        at.setToIdentity();
        at.translate((int)getX() + width()/2, (int)getY() + height()/2);
        at.rotate(Math.toRadians(getFaceAngle()));
        at.translate(-width()/2, -height()/2); //this line then moves to center
        at.scale(scaleX, scaleY); //scale AT THE END
        //at.translate(, ty);
    }

    public void draw() {
        g2d.drawImage(getImage(), at, applet);
    }

    //bounding rectangle
    public Rectangle getBounds() {
        Rectangle r;
        r = new Rectangle((int)getX(), (int)getY(), width(), height()); //do not forget to scale the bounding rectangle
        return r;
    }

}
